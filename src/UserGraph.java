import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Generate users and their relationships.
 */
public class UserGraph {
    ArrayList<User> users = new ArrayList<User>();
    ArrayList<Edge> edges = new ArrayList<Edge>(); //where we store relationships between users
    
    public UserGraph(int numberOfGroups, int avgSizeOfGroup) {
        Random rand = new Random(); //random integer generator
        int IDCounter = 0; //keeps track of current UserID we are generating
        
        //generate users and connections
        for (int x = 0; x < numberOfGroups; x++) {
            //determine the size of the group randomly
            int size = (int) rand.nextGaussian() + avgSizeOfGroup; //size of group is 1 to maxSizeOfGroup
            
            for (int y = 0; y < size; y++) {
                users.add(new User(IDCounter, 0));
                
                //add connection between groups
                int prob = rand.nextInt(100); //probability of a user being coached by someone in another group is 5%
                while (prob < 5 && x != 0){ //TODO: nobody in group 0 can coach a member in another group
                    //current user to connect to a already randomly generated user
                    int randCoach = rand.nextInt(IDCounter - size + 3);
                    int student = IDCounter;
                    edges.add(new Edge(randCoach, student));
                    
                    prob = rand.nextInt(100);
                }
                
                IDCounter++;
            }
            
            //add connections between users in this group
            //this needs to improve to better represent relationships
            if (size > 1) { //can't add edges if there is only one node
                int start = IDCounter - size;
                for (int y = start + 1; y < IDCounter; y++) {
                    edges.add(new Edge(start, y));
                }
            }
        }
    }
    
    public UserGraph(ArrayList<User> users, ArrayList<Edge> edges) {
        this.users = users;
        this.edges = edges;
    }
    
    public ArrayList<User> getUsers() {
        return users;
    }
    
    public ArrayList<Edge> getEdges() {
        return edges;
    }
    
    public int getSiteType(int user) {
        Iterator<User> itr = users.iterator();
        
        while (itr.hasNext()) {
            User u = itr.next();
            if (u.getUserID() == user) {
                return u.getSiteType();
            }
        }
        
        return 0; //this return statement should never be reached
    }
    
    public void setSiteType(int UserID, int type) {
        for (int x = 0; x < users.size(); x++) {
            if (users.get(x).getUserID() == UserID) {
                users.get(x).setSiteType(type);
                break;
            }
        }
    }
    
    public void add(User u) {
        users.add(u);
    }
    
    public String toString() {
        String s = "";
        if (users.size() == 1) {
            s += users.get(0).getUserID() + " (" + users.get(0).getSiteType() + ")\n";
        }
        Iterator<Edge> itr = edges.iterator();
        
        while (itr.hasNext()) {
            Edge e = itr.next();
            s += "C: " + e.getCoach() + " (" + this.getSiteType(e.getCoach()) + ")" + ", S: " + e.getStudent() + " (" + this.getSiteType(e.getCoach()) + ")\n";
        }
        return s;
    }
    
    /*
     * Prints to standard output the UserGraph.
     */
    public void print() {
        System.out.print(this.toString());
    }
}


public class User {
    int userID; //increments by 1 for each user (starting at 0)
    int siteType; //0 for old, 1 for new
    
    public User(int userID, int siteType) {
        this.userID = userID;
        this.siteType = siteType;
    }
    
    public int getUserID() {
        return userID;
    }
    
    public int getSiteType() {
        return siteType;
    }
    
    public void setSiteType(int siteType) {
        this.siteType = siteType;
    }
}

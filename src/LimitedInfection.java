import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.SingleGraph;

public class LimitedInfection {
    static final boolean VISUALIZE = false; //Do we want the visual version?
    
    static final int NUM_CLASSES = 100;
    
    static final int NUM_USERS_IN_CLASS = 10;
    
    static final int START_USER_ID = 0;
    
    static final int SLEEP_TIME = 50; //delay between each node in milliseconds for visual traverse
    
    static final int USERS_TO_INFECT = 700;
    
    int usersToInfect; 
    
    public static void main(String[] args) {
        UserGraph ug = new UserGraph(NUM_CLASSES, NUM_USERS_IN_CLASS);
        
        //choose which connected components we want to traverse
        ArrayList<CCData> cc = getConnectedComponents(ug.getEdges());
        Collections.sort(cc);
        cc = selectConnectedComponents(cc);
        
        if (VISUALIZE) {
            new Visualization(ug, cc, SLEEP_TIME);
        }
        else {
            new LimitedInfection(ug, cc, USERS_TO_INFECT);
            ug.print();
        }
    }
    
    public LimitedInfection(UserGraph ug, ArrayList<CCData> cc, int usersToInfect) {
        this.usersToInfect = usersToInfect;
        
        Graph graph = new SingleGraph("LimitedInfection");
        
        graph.setStrict(false);
        
        //create nodes
        ArrayList<User> users = ug.getUsers();
        Iterator<User> uitr = users.iterator();
        
        while(uitr.hasNext()) {
            User u = uitr.next();
            graph.addNode(u.getUserID() + "");
        }
        
        //create edges
        ArrayList<Edge> edges = ug.getEdges();
        Iterator<Edge> itr = edges.iterator();
        
        int counter = 0;
        while (itr.hasNext()) {
            Edge e = itr.next();
            graph.addEdge(counter++ + "", e.getCoach() + "", e.getStudent() + "", false);
        }

        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
        }
        
        //traverse only certain connected components
        for (int x = 0; x < cc.size(); x++) {
            traverse(graph.getNode(cc.get(x).getUserID() + ""), ug);
        }
    }
    
    private void traverse(Node source, UserGraph ug) {
        Iterator<? extends Node> k = source.getBreadthFirstIterator();

        while (k.hasNext()) {
            Node next = k.next();
            ug.setSiteType(Integer.parseInt(next.getAttribute("ui.label")), 1); //set the user's siteType to new
        }
    }
    
    /*
     * Should use dynamic programming approach here? Similar to subset sum problem?
     */
    private static ArrayList<CCData> selectConnectedComponents(ArrayList<CCData> cc) {
        int sum = 0;
        
        for (int x = 0; x < cc.size(); x++) {
            if (sum + cc.get(x).getNumNodes() <= USERS_TO_INFECT) {
                sum += cc.get(x).getNumNodes();
            }
            else {
                //remove big nodes
                for (int y = cc.size() - 1; y >= x; y--) {
                    cc.remove(y);
                }
                break;
            }
        }
        
        return cc; 
    }
    
    /*
     * Gets edges from UserGraph instance.
     * Keep track of visited nodes.
     * Set edges to null if they contain a node that exists in the list of visited nodes.
     */
    private static ArrayList<CCData> getConnectedComponents(ArrayList<Edge> ogEdges) {
        //copy edges since this is static and we don't want to manipulate original?
        ArrayList<Edge> edges = new ArrayList<Edge>();
        for (int x = 0; x < ogEdges.size(); x++) {
            int c = ogEdges.get(x).getCoach();
            int s = ogEdges.get(x).getStudent();
            edges.add(new Edge(c, s));
        }
        
        ArrayList<CCData> cc = new ArrayList<CCData>();

        while (edges.size() > 0) {
            //begin iteration through one connected component
            ArrayList<Integer> visitedNodes = new ArrayList<Integer>();
            //add first edge nodes to visitedNodes
            int c = edges.get(0).getCoach();
            int s = edges.get(0).getStudent();
            visitedNodes.add(c);
            visitedNodes.add(s);
            //store first node to store in CCData object
            int firstNode = c;
            edges.set(0, null);
            
            for (int x = 0; x < edges.size(); x++) {
                if (edges.get(x) != null) {
                    c = edges.get(x).getCoach();
                    s = edges.get(x).getStudent();
                
                    boolean remove = false;
                
                    if (visitedNodes.contains(c) && !visitedNodes.contains(s)) {
                        visitedNodes.add(s);
                        remove = true;
                    }
                    if (!visitedNodes.contains(c) && visitedNodes.contains(s)) {
                        visitedNodes.add(c);
                        remove = true;
                    }
                    if (visitedNodes.contains(c) && visitedNodes.contains(s)) {
                        remove = true;
                    }
                
                    if (remove) {
                        edges.set(x, null);
                        x = 0; //reset back to beginning
                    }
                }
            }

            CCData ccTemp = new CCData(visitedNodes.size(), firstNode);
            cc.add(ccTemp);
            
            edges.removeAll(Collections.singleton(null));
        }
        
        return cc;
    }
}

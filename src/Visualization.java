import java.util.ArrayList;
import java.util.Iterator;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

public class Visualization {
    static int startUserID;
    static int sleepTime;

    public Visualization(UserGraph ug, int startUserID, int sleepTime) {
        Graph graph = new SingleGraph("Infection");
        graph.setStrict(false);

        graph.addAttribute("ui.stylesheet", styleSheet);
        graph.setStrict(false);

        //create nodes
        ArrayList<User> users = ug.getUsers();
        Iterator<User> uitr = users.iterator();
        
        while(uitr.hasNext()) {
            User u = uitr.next();
            graph.addNode(u.getUserID() + "");
        }
        
        //create edges
        ArrayList<Edge> edges = ug.getEdges();
        Iterator<Edge> itr = edges.iterator();
        
        int counter = 0;
        while (itr.hasNext()) {
            Edge e = itr.next();
            graph.addEdge(counter++ + "", e.getCoach() + "", e.getStudent() + "", false);
        }
                
        graph.display();

        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
        }

        traverse(graph.getNode(startUserID + ""), ug);
    }
    
    public Visualization(UserGraph ug, ArrayList<CCData> cc, int sleepTime) {
        Graph graph = new SingleGraph("Infection");

        graph.addAttribute("ui.stylesheet", styleSheet);
        
        //create nodes
        ArrayList<User> users = ug.getUsers();
        Iterator<User> uitr = users.iterator();
        
        while(uitr.hasNext()) {
            User u = uitr.next();
            graph.addNode(u.getUserID() + "");
        }
        
        //create edges
        ArrayList<Edge> edges = ug.getEdges();
        Iterator<Edge> itr = edges.iterator();
        
        int counter = 0;
        while (itr.hasNext()) {
            Edge e = itr.next();
            graph.addEdge(counter++ + "", e.getCoach() + "", e.getStudent() + "", false);
        }
        
        graph.display();

        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
        }
        
        //traverse only certain connected components
        for (int x = 0; x < cc.size(); x++) {
            traverse(graph.getNode(cc.get(x).getUserID() + ""), ug);
        }
    }
    
    private void traverse(Node source, UserGraph ug) {
        Iterator<? extends Node> k = source.getBreadthFirstIterator();

        while (k.hasNext()) {
            Node next = k.next();
            ug.setSiteType(Integer.parseInt(next.getAttribute("ui.label")), 1); //set the user's site type to new
            next.setAttribute("ui.class", "marked");
            sleep();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(sleepTime);
        } catch (Exception e) {}
    }

    private String styleSheet =
        "node {" +
            "fill-color: black;" +
            "text-mode: hidden;" + //we can hide the node UserIDs with this on the graph
            "size: 5px;" +
        "}" +
        "node.marked {" +
            "fill-color: green;" +
        "}";
}


public class Edge {
    int coach;
    int student;
    
    /*
     * There can be an edge (1,2) and an edge (2,1).
     */
    public Edge(int coach, int student) {
        this.coach = coach;
        this.student = student;
    }
    
    public int getCoach() {
        return this.coach;
    }
    
    public int getStudent() {
        return this.student;
    }
}

import java.util.ArrayList;

/*
 * TotalInfection Test:
 * Goal:
 *     Make sure every node in a connected component becomes infected.
 * Test Cases:
 *     -1 user, 1 class (Test 1)
 *     -20 users, 10 users/class (Test 2)
 * LimitedInfection Test:
 * Goal: Make sure each entire connected component is infected.
 *       We should get as close as possible to the goal number of infected users.
 *       IMO, if one node in a connected component is infected, all other nodes should be infected.
 * Test Cases:
 *     -1 user, 1 class (Test 3)
 *     -20 users, 10 users/class (Test 4)
 *     
 * Instructions: Simply run this java program. Testing is automated.
 */

public class Tests {
    public static void main(String[] args) {
        //test TotalInfection
        ArrayList<User> u1 = new ArrayList<User>();
        u1.add(new User(0,0));
        ArrayList<Edge> e1 = new ArrayList<Edge>();
        
        UserGraph ug1 = new UserGraph(u1, e1);
        new TotalInfection(ug1);
        
        if (!ug1.toString().equals("0 (1)\n")) {
            System.out.println("Test 1 FAILED.");
            return;
        }
        
        ArrayList<User> u2 = new ArrayList<User>();
        u2.add(new User(0,0));
        u2.add(new User(1,0));
        u2.add(new User(2,0));
        u2.add(new User(3,0));
        u2.add(new User(4,0));
        u2.add(new User(5,0));
        u2.add(new User(6,0));
        u2.add(new User(7,0));
        u2.add(new User(8,0));
        u2.add(new User(9,0));
        u2.add(new User(10,0));
        u2.add(new User(11,0));
        u2.add(new User(12,0));
        u2.add(new User(13,0));
        u2.add(new User(14,0));
        u2.add(new User(15,0));
        u2.add(new User(16,0));
        u2.add(new User(17,0));
        u2.add(new User(18,0));
        u2.add(new User(19,0));
        
        ArrayList<Edge> e2 = new ArrayList<Edge>();
        e2.add(new Edge(0,1));
        e2.add(new Edge(0,2));
        e2.add(new Edge(0,3));
        e2.add(new Edge(0,4));
        e2.add(new Edge(0,5));
        e2.add(new Edge(0,6));
        e2.add(new Edge(0,7));
        e2.add(new Edge(0,8));
        e2.add(new Edge(0,9));
        e2.add(new Edge(10,11));
        e2.add(new Edge(10,12));
        e2.add(new Edge(10,13));
        e2.add(new Edge(10,14));
        e2.add(new Edge(10,15));
        e2.add(new Edge(10,16));
        e2.add(new Edge(10,17));
        e2.add(new Edge(10,18));
        e2.add(new Edge(10,19));
        
        UserGraph ug2 = new UserGraph(u2, e2);
        new TotalInfection(ug2);
        
        String result2 =
                "C: 0 (1), S: 1 (1)\n" +
                "C: 0 (1), S: 2 (1)\n" +
                "C: 0 (1), S: 3 (1)\n" +
                "C: 0 (1), S: 4 (1)\n" +
                "C: 0 (1), S: 5 (1)\n" +
                "C: 0 (1), S: 6 (1)\n" +
                "C: 0 (1), S: 7 (1)\n" +
                "C: 0 (1), S: 8 (1)\n" +
                "C: 0 (1), S: 9 (1)\n" +
                "C: 10 (0), S: 11 (0)\n" +
                "C: 10 (0), S: 12 (0)\n" +
                "C: 10 (0), S: 13 (0)\n" +
                "C: 10 (0), S: 14 (0)\n" +
                "C: 10 (0), S: 15 (0)\n" +
                "C: 10 (0), S: 16 (0)\n" +
                "C: 10 (0), S: 17 (0)\n" +
                "C: 10 (0), S: 18 (0)\n" +
                "C: 10 (0), S: 19 (0)\n";
        if (!ug2.toString().equals(result2)) {
            System.out.println("Test 2 FAILED.");
            return;
        }
        
        System.out.println("TotalInfection Tests PASSED.");
        
        //test LimitedInfection
        ArrayList<User> u3 = new ArrayList<User>();
        u3.add(new User(0,0));
        ArrayList<Edge> e3 = new ArrayList<Edge>();
        
        UserGraph ug3 = new UserGraph(u3, e3);
        //choose which connected components we want to traverse
        ArrayList<CCData> cc3 = new ArrayList<CCData>();
        cc3.add(new CCData(1,0));
        
        new LimitedInfection(ug3, cc3, 10);
        
        if (!ug3.toString().equals("0 (1)\n")) {
            System.out.println("Test 3 FAILED.");
            return;
        }
        
        ArrayList<User> u4 = new ArrayList<User>();
        u4.add(new User(0,0));
        u4.add(new User(1,0));
        u4.add(new User(2,0));
        u4.add(new User(3,0));
        u4.add(new User(4,0));
        u4.add(new User(5,0));
        u4.add(new User(6,0));
        u4.add(new User(7,0));
        u4.add(new User(8,0));
        u4.add(new User(9,0));
        u4.add(new User(10,0));
        u4.add(new User(11,0));
        u4.add(new User(12,0));
        u4.add(new User(13,0));
        u4.add(new User(14,0));
        u4.add(new User(15,0));
        u4.add(new User(16,0));
        u4.add(new User(17,0));
        u4.add(new User(18,0));
        u4.add(new User(19,0));
        
        ArrayList<Edge> e4 = new ArrayList<Edge>();
        e4.add(new Edge(0,1));
        e4.add(new Edge(0,2));
        e4.add(new Edge(0,3));
        e4.add(new Edge(0,4));
        e4.add(new Edge(0,5));
        e4.add(new Edge(0,6));
        e4.add(new Edge(0,7));
        e4.add(new Edge(0,8));
        e4.add(new Edge(0,9));
        e4.add(new Edge(10,11));
        e4.add(new Edge(10,12));
        e4.add(new Edge(10,13));
        e4.add(new Edge(10,14));
        e4.add(new Edge(10,15));
        e4.add(new Edge(10,16));
        e4.add(new Edge(10,17));
        e4.add(new Edge(10,18));
        e4.add(new Edge(10,19));
       
        UserGraph ug4 = new UserGraph(u4, e4);
        //choose which connected components we want to traverse
        ArrayList<CCData> cc4 = new ArrayList<CCData>();
        cc4.add(new CCData(10,0));
        new LimitedInfection(ug4, cc4, 15);
        
        String result4 =
                "C: 0 (1), S: 1 (1)\n" +
                "C: 0 (1), S: 2 (1)\n" +
                "C: 0 (1), S: 3 (1)\n" +
                "C: 0 (1), S: 4 (1)\n" +
                "C: 0 (1), S: 5 (1)\n" +
                "C: 0 (1), S: 6 (1)\n" +
                "C: 0 (1), S: 7 (1)\n" +
                "C: 0 (1), S: 8 (1)\n" +
                "C: 0 (1), S: 9 (1)\n" +
                "C: 10 (0), S: 11 (0)\n" +
                "C: 10 (0), S: 12 (0)\n" +
                "C: 10 (0), S: 13 (0)\n" +
                "C: 10 (0), S: 14 (0)\n" +
                "C: 10 (0), S: 15 (0)\n" +
                "C: 10 (0), S: 16 (0)\n" +
                "C: 10 (0), S: 17 (0)\n" +
                "C: 10 (0), S: 18 (0)\n" +
                "C: 10 (0), S: 19 (0)\n";
        if (!ug4.toString().equals(result4)) {
            System.out.println("Test 4 FAILED.");
            return;
        }
        
        System.out.println("LimitedInfection Tests PASSED.");
        
        System.out.println("All Tests PASSED.");
    }
}
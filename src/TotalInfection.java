import java.util.ArrayList;
import java.util.Iterator;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.SingleGraph;

/*
 * Every node in the connected component containing the specified UserID becomes infected.
 */
public class TotalInfection {
    static final boolean VISUALIZE = true; //Do we want the visual version?
    
    static final int NUM_CLASSES = 100;
    
    static final int NUM_USERS_IN_CLASS = 10;
    
    static final int START_USER_ID = 0;
    
    static final int SLEEP_TIME = 50;
    
    public static void main(String[] args) {
        UserGraph ug = new UserGraph(NUM_CLASSES, NUM_USERS_IN_CLASS);

        if (VISUALIZE) {
            new Visualization(ug, START_USER_ID, SLEEP_TIME);
        }
        else {
            new TotalInfection(ug);
            ug.print();
        }
    }
    
    public TotalInfection(UserGraph ug) {
        Graph graph = new SingleGraph("TotalInfection");
        
        graph.setStrict(false);

        //create nodes
        ArrayList<User> users = ug.getUsers();
        Iterator<User> uitr = users.iterator();
        
        while(uitr.hasNext()) {
            User u = uitr.next();
            graph.addNode(u.getUserID() + "");
        }
        
        //create edges
        ArrayList<Edge> edges = ug.getEdges();
        Iterator<Edge> eitr = edges.iterator();

        int counter = 0;
        while (eitr.hasNext()) {
            Edge e = eitr.next();
            graph.addEdge(counter++ + "", e.getCoach() + "", e.getStudent() + "", false);
        }

        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
        }

        traverse(graph.getNode(START_USER_ID + ""), ug);
    }
    
    private void traverse(Node source, UserGraph ug) {
        Iterator<? extends Node> k = source.getBreadthFirstIterator();

        while (k.hasNext()) {
            Node next = k.next();
            ug.setSiteType(Integer.parseInt(next.getAttribute("ui.label")), 1); //set the user's siteType to new
        }
    }
}


/*
 * When we are search through the connected components of the graph,
 * we will be keeping track of the size of the connected component
 * and the first node that we discover in the connected component.
 * 
 * Later when we need to infect the connected component, we will use
 * userID to infect the first node and then traverse across the component
 * breadth first infecting the rest of the nodes.
 */
public class CCData implements Comparable<CCData> {
    int numberOfNodes;
    int userID; //a single userID in the component
    
    public CCData(int numberOfNodes, int userID) {
        this.numberOfNodes = numberOfNodes;
        this.userID = userID;
    }
    
    public int getNumNodes() {
        return numberOfNodes;
    }
    
    public int getUserID() {
        return userID;
    }
    
    @Override
    public int compareTo(CCData cc) {
        if (cc.getNumNodes() > this.getNumNodes()) {
            return -1;
        }
        else if (cc.getNumNodes() == this.getNumNodes()) {
            return 0;
        }
        else {
            return 1;
        }
    }
}
